<?php

namespace SMSTalk;

class Basic {
    

    public static function send($destino, $mensagem, $agendamento = null, $conta_id = '', $campanha_id = '') {
        
        if (11 > strlen($destino) || 15 < strlen($destino)) {
            throw new \Exception("Destinatário inválido.");
        }
        if ('' == $mensagem) {
            throw new \Exception("Mensagem vazia.");
        }

        if (null == $agendamento) {
            $agendamento = date('d/m/Y\+H:i:s');
        }
        $params = array(
            'ContaId' => $conta_id,
            'CampanhaId' => $campanha_id,
            'From' => '',
            'To' => $destino,
            'Msg' => urlencode($mensagem),
            'Agendamento' => $agendamento,
            'Id' => uniqid(),
            'RespostaOpcao' => 1
        );

        //return Api::send('EnvioSimples/EnviarJson', $params);
        return Api::send('EnvioSimples/Get', $params, true);
    }

    public static function get($codigo_integracao) {
        if ('' == $codigo_integracao) {
            throw new \Exception("Código de integração vazio.");
        }

        $params = array('CodigoIntegracao' => $codigo_integracao);
        return Api::send('ConsultaSimples/ConsultarJson', $params);
    }
    
}