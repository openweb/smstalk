<?php

namespace SMSTalk;

use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\TooManyRedirectsException;
use GuzzleHttp\Psr7;
use GuzzleHttp\Middleware;

class Api {
    public static $usuario = null;
    public static $senha = null;

    public static function setup($usuario, $senha) {
        static::$usuario = $usuario;
        static::$senha = $senha;
    }

    public static function setUsuario($usuario) {
        static::$usuario = $usuario;
    }

    public static function setSenha($senha) {
        static::$senha = $senha;
    }

    public static function getUsuario() {
        return static::$usuario;
    }

    public static function getSenha() {
        return static::$senha;
    }

    public static function send($url, $params, $get = false) {
        if (null == static::getUsuario()) {
            throw new \Exception("Usuário não informado.");
        }
        if (null == static::getSenha()) {
            throw new \Exception("Senha não informada.");
        }

        $params['Usuario'] = static::getUsuario();
        $params['Senha'] = static::getSenha();

        try {
            
            if ($get) {
                $query = array();
                foreach($params as $key=>$value) {
                    $query[] = "{$key}={$value}";
                }
                $query = join('&', $query);
                
                ///$request = $client->request('GET', "http://secure.talktelecom.com.br/api/{$url}?{$query}",array('handler' => $tapMiddleware($clientHandler)));
                $body = @file_get_contents("http://secure.talktelecom.com.br/api/{$url}?{$query}");
                if (FALSE === $body) {
                    throw new \Exception("Não foi possível enviar a solicitação.");
                }
            } else {
                $client = new \GuzzleHttp\Client();
                $request = $client->request('POST', "http://secure.talktelecom.com.br/api/{$url}",array('json' => $params));
                $body = $request->getBody();
                if (preg_match('/json/',$request->getHeaderLine('content-type'), $m)) {
                    $body = json_decode($body);
                }
            }
    

        } catch (ClientException $e) {
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $body = $response->getBody().'';
                if (preg_match('/json/',$response->getHeaderLine('content-type'), $m)) {
                    $body = json_decode($body);
                    if (isset($body->Message)) {
                        throw new \Exception($body->Message);
                    }
                } else {
                    throw new \Exception($body);
                }
            }
            throw new \Exception($e->getMessage());
        }

        return $body;
    }
}