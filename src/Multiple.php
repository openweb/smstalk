<?php

namespace SMSTalk;

class Multiple {
    public static function send($mensagens) {
        $params = array_map($mensagens, function ($obj) {
            $obj = (object)$obj;
            if (!isset($obj->agendamento)) {
                $obj->agendamento = date('d/m/Y H:i:s');
            }
            return array(
                'ContaId' => $obj->conta_id,
                'CampanhaId' => $obj->campanha_id,
                'NA' => '',
                'NB' => $obj->destino,
                'Mensagem' => $obj->mensagem,
                'DataInicio' => $obj->agendamento,
                'CodigoIntegracao' => uniqid(),
                'RespostaOpcao' => 0
            );
            
        });

        return Api::send('EnvioMultiplo/EnviarJson');
    }

    public static function get($codigos) {
        if (empty($codigos)) {
            throw new \Exception("Lista de códigos de integração vazia;");
        }
        $params = array(
            'ListaCodigoIntegracao' => $codigos
        );
        return Api::send("ConsultaMultiplo/ConsultarJSON", $params);
    }
}