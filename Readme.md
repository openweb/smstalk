# SMSTalk

## Instalation
```
    composer config repositories.smstalk vcs https://bitbucket.org/openweb/smstalk
    composer require openweb/smstalk
```

## Quickstart

### Send an SMS
```php
    SMSTalk\Api::setup("username", "password");
    try {
        SMSTalk\Basic::send("+5551999999999", "Lorem ipsum dolor.");
        echo "OK";
    } catch (Exception $e) {
        echo "Error: ".$e->getMessage();
    }

```
